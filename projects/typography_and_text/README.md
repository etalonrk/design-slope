# Typography and Text
Sketch let you add simple and plain text and also gives you the power to create a design of it.
Change color, font, style... do you wanna your text follows a path... you can do it as easy as
click a couple on options.

## Demo
![typography_text_sketch_ui](.screenshots/typography_text_sketch_ui.png)
