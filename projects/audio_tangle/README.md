# Audio Tangle

## What is the Goal of your App?
Audio Tangle is a marketplace that allows musicians to find producers to work with in their immediate proximity while facilitating bid prices from producers.

## Describe you app as if it were a person
Friendly, easy going, modern.

## Who will be using your app?
Music professionals ages 20-50. Part time or full time musicians.
