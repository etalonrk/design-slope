# Symbols
Create a design could be as easy as you want, for that... Sketch has a functionality called `Symbol`
Creating a symbol will let us use a same part of design to replicate it on all our design, those symbols, could be `Buttons`, `Navigation Bars`, `Icons`... whatever you think is gonna be exactly the same on your whole design.

## Demo
### Sketch UI
![instaslam_sketch_ui](.screenshots/instaslam_sketch_ui.png)

### Exported
![instaslam_demo](.screenshots/instaslam_demo.png)
