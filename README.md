<p align="center">
  <img src=".assets/logo.png" title="Logo">
</p>

---

[![ide_sketch](.assets/ide.png)](https://www.sketchapp.com)
[![twitter](.assets/twitter.png)](https://twitter.com/fdorado985)
[![instagram](.assets/instagram.png)](https://www.instagram.com/juan_fdorado)

On this repo we are going to save some little designs, that we help us to learn the most basics steps of `Design` usign `Sketch`.

I invite you to take a look at them

Now remember... each time that you think on create a new app... answer these three questions first.

* What is the goal?
* Describe the app as if it were a person
* Who is using this?

Once you are able to answer the questions before... you are ready to start your design.

Here are the designs and a little description of what are they made it for. Enjoy it and most important KEEP DESIGNING!

## Mastering Mobile App With Sketch
* Basics of Sketch
  * [Super App](projects/super_app) - Designing your first App.
  * [Shapes](projects/shapes) - Understanding `Shapes`.
  * [Masking & Scaling](projects/masking_scaling) - `Masking` & Proportional `Scaling`.
  * [Styling](projects/styling) - Frames, Corner Radius, Colors and more...
  * [Typography & Text](projects/typography_and_text) - Add and modify text.
  * [Symbols](projects/symbols) - Create `Symbols` and export them.
* Advancing Your Skills
  * [Chat App](projects/chat_app) - Create a simple UI for a `Chat App` using all the basics.
* Designing an App with Requirements
  * [Globetrotter](projects/globetrotter) - Create a little bit more complex UI design following requirements.

## Mobile Product Design: From Napkin To Launch
* [TopTen](projects/topten) - Intro to UI & UX
* [Audio Tangle](projects/audio_tangle) - Complex UI & UX
