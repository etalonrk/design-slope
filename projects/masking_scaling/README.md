# Masking & Scaling

Sketch has some amazing functionalities like `Mask` and `Scale`, those functionalities are so good for example in this case, create a profile image using a `Mask` to make an image rounded.

Also... `Scale` let us keep the aspect ratio between our original masked image and our new scaled image.

## Demo
![masking_scaling_sketch_ui](.screenshots/masking_scaling_sketch_ui.png)
