# Chat App
Here you will see an app design fully created with Sketch
You are gonna be able to see 4 screens that cover...
* Login UI
* Settings UI
* Messages UI
* New Message UI
* Conversation UI

## Demo
![chat_app_demo](.screenshots/chat_app_demo.png)
