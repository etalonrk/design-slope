# TopTen

## What is the goal of your app?
The goal is to keep the user up to date on a topic they care about in a brief and engaging manner.

## Describe your app as if it were a person
Modern, credible, deliberate

## Who will be using your app?
20-30 year old busy young professionals.

## Demo
![topten_demo](.screenshots/topten_demo.png)
